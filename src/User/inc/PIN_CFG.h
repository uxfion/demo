#ifndef _PIN_CFG_H_
#define _PIN_CFG_H_

#define ON	1
#define OFF	0
//----LED引脚----	 
#define  OLED_SCL_PIN	B15
#define  OLED_SDA_PIN	A5
#define  OLED_RST_PIN	A19
#define  OLED_DC_PIN	A6
#define  OLED_CS_PIN	0xff

//----MPU6050模拟IIC引脚-----
#define MPU6050_SCL_PIN     A14
#define MPU6050_SDA_PIN     A13


#define MCP41_SCK		A21
#define MCP41_SDA		A20


//-----------电感--------------

#define AD_CH_L__				ADC_CH0_A10  	//1 D5
#define AD_CH_L_1				ADC_CH5_A31  	//2 E4
#define AD_CH_M_1				ADC_CH3_A15 	//3 D4
#define AD_CH_R_1				ADC_CH6_B0		//4  D1
#define AD_CH_R__				ADC_CH4_A16   //5  E6
#define AD_CH_L_2       0xff//ADC_CH1_A11   //6  B21
#define AD_CH_R_2       0xff//ADC_CH2_A12   //7 B21


#define CS0		B19  
#define CS1		B10
#define CS2		B7
#define CS3		B22
#define CS4		B9
#define CS5  	0xff//B27
#define CS6		0xff//B20
#define CS7		0xff

#define p0		AD_CH_L__
#define p1		AD_CH_L_1
#define p2		AD_CH_M_1
#define p3		AD_CH_R_1
#define p4		AD_CH_R__
#define p5		0xff
#define p6		0xff
#define p7		0xff

//----按键----
/******************
#define BUTTON_UP    B26
#define BUTTON_DOWN  B13
#define BUTTON_LEFT  B28  
#define BUTTON_RIGHT B12
#define BUTTON_MID   B27
*******************/
/********板子翻转**********/
#define BUTTON_UP    B13
#define BUTTON_DOWN  B26
#define BUTTON_LEFT  B12 
#define BUTTON_RIGHT B28
#define BUTTON_CONFIRM   B27


//----拨码开关----
#define SWITCH1_1 A1
#define SWITCH1_2 B4	
#define SWITCH1_3 B5 
#define SWITCH1_4 B6

#define SWITCH2_1 B23
#define SWITCH2_2 B24	
#define SWITCH2_3 B2 
#define SWITCH2_4 B25

//----蜂鸣器----
#define BUZZER   B21

//电机PWM
#define PWM_CH_R_Z			SCT0_OUT6_A27
#define PWM_CH_R_F			SCT0_OUT7_A28
#define PWM_CH_L_Z			SCT0_OUT9_A30
#define PWM_CH_L_F		  SCT0_OUT8_A29

//编码器
#define CODER_L         TIMER3_COUNT0_A4
#define CODER_R         TIMER0_COUNT1_A2
#define CODER_DIR_L     B30
#define CODER_DIR_R     A3

////----------串口---------
#define Bluetooth_UART     USART_8
#define Bluetooth_UART_TXD UART8_TX_B18
#define Bluetooth_UART_RXD UART8_RX_B17

//----------外设------------
#define	Laser_Hengduan		ADC_CH11_A23//(ADC)				
#define	Laser_Podao				A24//激光
#define	Laser_Duanlu			A9
#define Reed_Switch  			A25
#endif
