#ifndef _SELFBUILD_CONTROL_H_
#define _SELFBUILD_CONTROL_H_

#include "headfile.h"

#define Dead_PWM	100		//死区PWM
#define PWM_MAX 	1000	//最大PWM
#define ZERO_Pitch	34		//机械零点

extern float pitch,yaw,roll;
extern float gyrox,gyroy,gyroz;

extern uint8 System_OK;

typedef struct 	//电机相关数据结构体
{
	int32 Speed_Ave_Set;	
  
	int8 Left_Crazy;	// 电机疯转
	int8 Right_Crazy;	// 电机疯转
	
	int8 Crazy_StopFlag;
  
	int32 Speed_L;
	int32 Speed_L_Last;
	int32 Left_Acc;
	
	int32 Speed_R;
	int32 Speed_R_Last;
	int32 Right_Acc;
	
	int32 Speed_Ave_Now;
	int32 Speed_Ave_Last;
	int32 Crazy_Count;
	
	int8 Car_Direct;		//车运动方向
	
	
	int32 PWM_R_Set;
	int32 PWM_L_Set;
	
	int32 PWM_R_Set_Last;
	int32 PWM_L_Set_Last;
	
}Motor_Typedef;

extern Motor_Typedef Motor;	//电机相关变量


typedef struct
{
	uint8 Time_out,
	Steer_crazy,
	Wireless_Stop,
	Text_Stop;
}Error_Flag_Typedef;

extern Error_Flag_Typedef Error_Flag; //错误相关变量


uint8 init_all(void);
uint8 Speed_Get(void);
uint8 PWM_Set(void);
void DataRead(void);
#endif
