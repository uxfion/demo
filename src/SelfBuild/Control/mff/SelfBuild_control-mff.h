#ifndef _SELFBUILD_CONTROL_H_
#define _SELFBUILD_CONTROL_H_

#include "headfile.h"

uint8 Init_ALL(void);
int32 range_protect(int32 duty, int32 min, int32 max);
//***********按键控制***********
void Button_control(void);
//*************速度控制**********
void Speed_control(void);
//************斷路判断************	
void Duanlu_judge(void);
extern uint8 Duanlu_flag;
extern uint8 PoDao_flag;
//************横断坡道判断************	
//-----横断--------
double Hengduan_Podao_judge(void);
extern uint8 Hengduan_flag;
extern double Hengduan_distance;
extern float Remeber_yaw;;
extern int16 Hengduan_DIR[3];
//***********起跑线判断************
void  Start_Line(void);
#endif
