#ifndef _SELFBUILD_PID_H_
#define _SELFBUILD_PID_H_

#include "common.h"

typedef struct 
{
  	float Param_Kp;
	float Param_Ki;
	float Param_Kd;
	
    float AidData_Set;
	float Error_Sum;
	float Param_measure;
	
	float Error;
	float Error_last;
	float Error_last_last;
	
	int32 PID_Dynam_Out;
	int32 PID_Incre_Return;
	int32 PID_Local_Out;
	
}PID_Param_Set;

typedef enum
{
	Dynam,
	Incre,
	Local,
}PID_Mode_Typedef;

extern PID_Param_Set PID_Ang_Velo;	//角速度环PID参数
extern PID_Param_Set PID_Ang;		//角度环PID参数
extern PID_Param_Set PID_Speed;	//速度环PID参数
extern PID_Param_Set PID_Direct[2];	//转向环PID参数
extern PID_Param_Set PID_Elec;	//电磁循迹PID参数
extern PID_Param_Set PID_Camer[2];//摄像头循迹PID参数
extern PID_Param_Set PID_Stop;	//电磁循迹PID参数

extern float Turn[5][4];

int16 PID_Calcu	(float Aid_Data,float Measure_Data,PID_Param_Set* P_I_D,PID_Mode_Typedef PID_Mode);

#endif

