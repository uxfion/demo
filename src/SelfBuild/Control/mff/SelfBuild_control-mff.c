#include "SelfBuild_control.h"

//******************************************
//  @备注         全车初始化函数
//******************************************
uint8 Init_ALL(void)		//初始化
{
//  /*----------------------------按键--------------------------------*/
		gpio_init(Button_Up, GPI, 1,PULLUP);
		gpio_init(Button_Down, GPI, 1,PULLUP);
		gpio_init(Button_Left, GPI, 1,PULLUP);
		gpio_init(Button_Right, GPI, 1,PULLUP);
		gpio_init(Button_Mid, GPI, 1,PULLUP);		
	
//	/*----------------------------蜂鸣器--------------------------------*/

		gpio_init(Beep, GPO, 0,PULLDOWN);
//	/*----------------------------拨码开关--------------------------------*/
		gpio_init(Switch1_1, GPI, 1,PULLUP);
		gpio_init(Switch1_2, GPI, 1,PULLUP);
		gpio_init(Switch1_3, GPI, 1,PULLUP);
		gpio_init(Switch1_4, GPI, 1,PULLUP);
		
		gpio_init(Switch2_1, GPI, 1,PULLUP);
		gpio_init(Switch2_2, GPI, 1,PULLUP);
		gpio_init(Switch2_3, GPI, 1,PULLUP);
		gpio_init(Switch2_4, GPI, 1,PULLUP);
		systick_delay_ms(100);
//  /*----------------------------外设--------------------------------*/
	
		gpio_init(Laser_Duanlu,GPI, 1,PULLUP);//断路
		gpio_init(Laser_Podao,GPI, 1,PULLUP);//横断
		gpio_init(Reed_Switch,GPI,1,PULLUP);
		adc_init(Laser_Hengduan);
//  /*----------------------------OLED初始化--------------------------------*/
    oled_init(); 	
	
//	 /*----------------------------ADC采样--------------------------------*/
		adc_init(AD_CH_L__);
		adc_init(AD_CH_L_1);
		adc_init(AD_CH_M_1);
		adc_init(AD_CH_R_1);
		adc_init(AD_CH_R__);
	 
// /*----------------------------编码器初始化------------------------------*/
	  ctimer_count_init(CODER_L);	
		ctimer_count_init(CODER_R);
		gpio_init(CODER_DIR_L, GPI, 1,PULLUP);
		gpio_init(CODER_DIR_R, GPI, 1,PULLUP);
		
/*----------------------------电机初始化-------------------------------*/
		sct_pwm_init(PWM_CH_R_Z, 7500, 0);
		sct_pwm_init(PWM_CH_R_F, 7500, 0);
		sct_pwm_init(PWM_CH_L_Z, 7500, 0);	
		sct_pwm_init(PWM_CH_L_F, 7500, 0);

//  /*----------------------------六轴初始化--------------------------------*/ 
		while(MPU6050_ADDDMPInit())  systick_delay_ms(10);	
	
//  /*----------------------------菜单调参----------------------------------*/
		DisableInterrupts;                          //关闭所有中断，防止菜单调节过程中出现中断
		Menu_Init();                                  //初始化菜单
		while(!Menu_Work()) systick_delay_ms(200);    //菜单每200ms工作一次，并根据是否按下“关闭菜单”选项后（函数返回0）结束死循环
  	EnableInterrupts;
	
//  /*----------------------------数字电位器初始化--------------------------*/
		Correct_Sensor();
		
//	/*----------------------------定时器中断初始化--------------------------------*/
		mrt_pit_init_ms(MRT_CH0,2);
		mrt_pit_init_ms(MRT_CH1,10);
		mrt_pit_init_ms(MRT_CH2,10);
		set_irq_priority(MRT0_IRQn,2);	
		
//	/*-------------------------------------初始化全部结束--------------------------------------------------------------------------------*/ 
     return SUCCESS;
}


void Button_control()
{
	static uint8 Oled_fill_flag = 0;
	int8 BM_Value = 0;
	
	//************拨码开关***************
	//左边竖直

	//右边平行
	if(gpio_get(Switch2_1)==0)	BM_Value |= 0x01;
	else if(gpio_get(Switch2_2)==0)	BM_Value |= 0x02;
	else if(gpio_get(Switch2_3)==0)	BM_Value |= 0x04;
	else if(gpio_get(Switch2_4)==0)	BM_Value |= 0x08;
	else BM_Value |= 0x00;
		
	if(Oled_fill_flag != BM_Value)
	{
		Oled_fill_flag =BM_Value;
		oled_fill(0x00);
	}
	if(BM_Value == 0x01)
	{
			OLED_P6x8Int(OLED_SHOW(1), 2, adc_convert(AD_CH_L__, ADC_8BIT), 3);
			OLED_P6x8Int(OLED_SHOW(2), 2, adc_convert(AD_CH_L_1, ADC_8BIT), 3);
			OLED_P6x8Int(OLED_SHOW(3), 2, adc_convert(AD_CH_M_1, ADC_8BIT), 3);
			OLED_P6x8Int(OLED_SHOW(4), 2, adc_convert(AD_CH_R_1, ADC_8BIT), 3);//显示电感读数
			OLED_P6x8Int(OLED_SHOW(5), 2, adc_convert(AD_CH_R__, ADC_8BIT), 3);
			
			OLED_P6x8Int(OLED_SHOW(1), 4, AD_Data.AD_L_L, 3);
			OLED_P6x8Int(OLED_SHOW(2), 4,	AD_Data.AD_L, 3);
			OLED_P6x8Int(OLED_SHOW(3), 4, AD_Data.AD_M, 3);
			OLED_P6x8Int(OLED_SHOW(4), 4, AD_Data.AD_R, 3);		
			OLED_P6x8Int(OLED_SHOW(5), 4, AD_Data.AD_R_R, 3);//显示电感读数
	}	
	else if	(BM_Value == 0x02)	
	{
			OLED_P6x8Str(OLED_SHOW(2), 1, "pitch");
			OLED_P6x8Flo(OLED_SHOW(4), 1, pitch, -3);
			OLED_P6x8Str(OLED_SHOW(2), 2, "roll");
			OLED_P6x8Flo(OLED_SHOW(4), 2, roll, -3);
			OLED_P6x8Str(OLED_SHOW(2), 3, "yaw");
			OLED_P6x8Flo(OLED_SHOW(4), 3, yaw, -3);
			OLED_P6x8Str(OLED_SHOW(2), 4, "Y");
			OLED_P6x8Int(OLED_SHOW(4), 4, g_fGyroYRead, -4);
			OLED_P6x8Str(OLED_SHOW(2), 5, "Z");
			OLED_P6x8Int(OLED_SHOW(4), 5, g_fGyroZRead, -4);
	    OLED_P6x8Str(OLED_SHOW(2), 6, "X");
			OLED_P6x8Int(OLED_SHOW(4), 6, g_fGyroXRead, -4);
	}
	else if (BM_Value == 0x04)
	{
			Normalized_MCP41();
	}
	else if (BM_Value == 0x08)
	{
			OLED_P6x8Int(OLED_SHOW(1), 2, Flag_Huandao, 3);
			OLED_P6x8Int(OLED_SHOW(3), 2, GYROZ_SUM, 3);
			OLED_P6x8Int(OLED_SHOW(1), 4, (Hengduan_distance), 5);
			OLED_P6x8Int(OLED_SHOW(1), 5, (adc_convert(ADC_CH11_A23,ADC_12BIT)), 5);
			OLED_P6x8Int(OLED_SHOW(1), 6, (adc_convert(ADC_CH11_A23,ADC_12BIT))*3300/4096, 5);
	}
	else if(BM_Value == 0x00)
	{
			oled_fill(0x00);
	}
		
	//******************按键开关*****************
	 static int16 start_time=250;
	 static int8  Flag_Star=0; 
	
	if(gpio_get(Button_Mid)==0 && Flag_Star==OFF)
			Flag_Star = ON;
	if(Flag_Star == ON)
	{
			if(start_time > 0)
			{
				start_time--;
			}
			else
			{
				Flag_Run=ON;
				Flag_Star=OFF;
			}
	}
	
	if(Flag_Stop == ON)
	{
		Flag_Run =OFF;
			if(Speed_Now>10)
			{
			sct_pwm_duty(PWM_CH_L_Z, 0);
			sct_pwm_duty(PWM_CH_L_F, 800);
			sct_pwm_duty(PWM_CH_R_Z, 0);	
			sct_pwm_duty(PWM_CH_R_F, 800);	
			}
			else
			{
			sct_pwm_duty(PWM_CH_L_Z, 0);
			sct_pwm_duty(PWM_CH_L_F, 0);
			sct_pwm_duty(PWM_CH_R_Z, 0);	
			sct_pwm_duty(PWM_CH_R_F, 0);	
			}
	}
}
//************************速度控制*****************
void Speed_control()
{
	if(Flag_Run)
	{
		if(Flag_Huandao != 0)
		{
			if(Flag_zitai == 0)	 Speed_Set = Speed_zhili-50;
			else if(Flag_zitai == 1)	 Speed_Set = Speed_sanlun - 30;
		}
		else
		{
			if(Flag_zitai == 0)	 Speed_Set = Speed_zhili;
			else if(Flag_zitai == 1)	 Speed_Set = Speed_sanlun;
		}
		
		if(Hengduan_flag!= 0)
		{
			if(Flag_zitai == 0)	 Speed_Set = Speed_zhili;
			else if(Flag_zitai == 1)	 Speed_Set = 100;
		}
		else
		{
			if(Flag_zitai == 0)	 Speed_Set = Speed_zhili;
			else if(Flag_zitai == 1)	 Speed_Set = Speed_sanlun;
		}
	}
}
//************斷路判断************	
uint8 Duanlu_flag = 0;
void Duanlu_judge()
{
		static uint8 time_count1=0;
		static uint8 time_count2=0;
		static uint8 time_count3=0; 
	
if(Hengduan_flag == 0)
{	
	 if((gpio_get (Laser_Duanlu) == 1)  && (Duanlu_flag == 0)  &&  (Flag_zitai == 0)) 	time_count1++ ;
	 if((gpio_get (Laser_Duanlu) == 1)  &&(time_count1>25) && (Duanlu_flag == 0)  &&  (Flag_zitai == 0))//看到断路
	 {
			time_count1 = 0;
			Duanlu_flag = 1;
			Flag_zitai = 1;
		  Flag_Huandao= 0;
	 }
	 
	 if((gpio_get (Laser_Duanlu) == 1)  && (Duanlu_flag == 0)  &&  (Flag_zitai == 1))		time_count2++ ;	
	 if ((gpio_get (Laser_Duanlu) == 1) &&(time_count2> 25) && (Duanlu_flag == 0)  &&  (Flag_zitai == 1))
	{
			time_count2 = 0;
			Duanlu_flag = 1;
			Flag_zitai = 0;
			Flag_Huandao= 0;
	}
	 
   if((gpio_get (Laser_Duanlu) == 0)  && (Duanlu_flag == 1)) time_count3++;
	 if((gpio_get (Laser_Duanlu) == 0)  &&(time_count3 > 100)&& (Duanlu_flag == 1))
	 {
		  time_count3 = 0;
			Duanlu_flag = 0;
	 }
 }	 
}

//************横断坡道判断************	
//-----横断--------
#define Traverse_x 0.6	
#define Traverse_y 0.5
#define Traverse_straight 0.5
uint8 Hengduan_flag = 0;
uint8 PoDao_flag=0;
double Hengduan_distance = 0; //距离
float Remeber_yaw = 0;
int16 Hengduan_DIR[3]={0,0,0};

double Hengduan_Podao_judge()//看到是0 正常是1
{
		static uint8 time_count1 = 0;
		static double x;
	  static uint16 Traverse_Flag=0;
		static uint8 HengDuan_time=0;//横断次数
  	static double distance_Hengduan_Sum =0;//路程
 if(Traverse_Flag==0)
 {	
	 if(Flag_zitai==1)
	 {

	   if(Hengduan_distance <=60 && (Hengduan_flag ==0)&&Hengduan_DIR[HengDuan_time]!=0)//看到横断
	   {
			  Hengduan_flag  = 1;
		
	   }	
		 if(Hengduan_distance <=60  && (Hengduan_flag ==0)&&Hengduan_DIR[HengDuan_time]==0)
    {	
			    PoDao_flag=1;
			    Traverse_Flag = 6;
		 
	  }
   }

	 else if(Flag_zitai==0)
	 {
		 if((gpio_get(Laser_Podao) == 0) && (time_count1++ > 3) && (Hengduan_flag  ==0)&&pitch>20.00f&&Hengduan_DIR[HengDuan_time]!=0) //看到横断
		{
				time_count1 = 0;
				Hengduan_flag  = 1;
		}
		if((gpio_get(Laser_Podao) == 0) && (time_count1++ > 3) && (Hengduan_flag ==0)&&pitch>20.00f&&Hengduan_DIR[HengDuan_time]==0) //看到横断
		{
				  PoDao_flag=1;
			    Traverse_Flag = 6;
		}
	}
  
}
	
	
/**********************坡道*****************************************/	
	if(Traverse_Flag == 6 && distance_Hengduan_Sum >1.60f)  //坡道误判处理
	 {
	    Traverse_Flag = 0;
		  distance_Hengduan_Sum = 0;
		  PoDao_flag=0;
		  HengDuan_time++;
	 }
	

	 if(Traverse_Flag == 0&&	Hengduan_flag ==1)//识别到横断
	 {
		 
		 Traverse_Flag=1;
		 Remeber_yaw=yaw;
	 }
	 if(Traverse_Flag !=0)
		 distance_Hengduan_Sum += Speed_Now * 0.000181278f;//编码器记录下路程
	 
	 if(Traverse_Flag ==1 
		&& ((distance_Hengduan_Sum  <= sqrt(Traverse_x*Traverse_x*0.25+Traverse_y*Traverse_y))
		&& (distance_Hengduan_Sum ) >= 0))                                                        //第一段
	 {
	    Traverse_Flag = 2;
		  x=((Traverse_y*2)/(float)Traverse_x) * Hengduan_DIR[HengDuan_time];
	 }

	 if(   Traverse_Flag == 2                                                
		&& ((distance_Hengduan_Sum ) <= (sqrt(Traverse_x*Traverse_x*0.25f+Traverse_y*Traverse_y)+Traverse_straight))
	  && ((distance_Hengduan_Sum ) > sqrt(Traverse_x*Traverse_x*0.25f+Traverse_y*Traverse_y))) //第二段走直线
	 {   Traverse_Flag = 3;
		   x=0;
	 }
	 	
	 if(  Traverse_Flag == 3
		 && (distance_Hengduan_Sum )>(sqrt(Traverse_x*Traverse_x*0.25f+Traverse_y*Traverse_y)+Traverse_straight))  //第三段
	 {
	    Traverse_Flag = 4;
		  x=(-((Traverse_y*2)/(float)Traverse_x)+0.5f*Hengduan_DIR[HengDuan_time]) * Hengduan_DIR[HengDuan_time];
	 }

	 if(Traverse_Flag == 4&&(gpio_get (Laser_Duanlu) == 0))  //再走过一定的路重置标志位
	 {
	    Traverse_Flag = 0;
		  Hengduan_flag=0;
		  distance_Hengduan_Sum = 0;
		  HengDuan_time++;
	 }
	 
	 
	 return atan(x)*57.295779;
		
	}

//**************起跑线检测******************
void  Start_Line(void)
{
	static uint8 Ready_Stop = 0;
  static uint8 Start_OK = 0;	
	static uint16 No_Start_Line_Count = 0;
	uint8 Starting_Line_Flag = 0;
	
	if(Flag_Run)
	{	//开始
		if(gpio_get(Reed_Switch)==0) Starting_Line_Flag=1;
		else  Starting_Line_Flag=0;
		
		if (!Starting_Line_Flag && !Start_OK)	//没有检测到起跑线且未成功出发
		{
			No_Start_Line_Count++;			
			if (No_Start_Line_Count >= 300)	//连续n次没有检测到起跑线
			{
				No_Start_Line_Count = 0;
				Start_OK = 1;	//出发成功
			}
		}
		//结束
		if (Start_OK && Starting_Line_Flag)//成功发车且检查到起跑线
		{
			Ready_Stop = 1;//准备停车
		}
		if(Ready_Stop)
			{
				No_Start_Line_Count = 0;		
				Flag_Stop = ON;    //停车
				Start_OK = 0;			//清除出发成功标志位
				Ready_Stop = 0;			//清除准备停车标志位
			}
	}
}
 

//******** 限幅保护 *********//
int32 range_protect(int32 duty, int32 min, int32 max)//限幅保护
{
	if (duty >= max)
	{
		return max;
	}
	if (duty <= min)
	{
		return min;
	}
	else
	{
		return duty;
	}
}
