   /*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2018,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：179029047
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		isr
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看LPC546XX_config.h文件内版本宏定义
 * @Software 		IAR 7.8 or MDK 5.24a
 * @Target core		LPC54606J512BD100
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2018-05-24
 ********************************************************************************************************************/

#include "headfile.h"
#include "isr.h"

//uint8 FLAG_2MS=0,FLAG_5MS=0,FLAG_10MS=0,FLAG_50MS=0,FLAG_100MS=0,FLAG_200MS=0;
uint16 Stop_Time;

void RIT_DriverIRQHandler(void)
{
	/***********yyz code***************
    PIT_FLAG_CLEAR;
    static uint8 Add_2ms,Add_5ms,Add_10ms,Add_50ms,Add_100ms,Add_200ms;
	static uint16 Timer;
    Add_2ms++;
    Add_5ms++;
    Add_10ms++;
    Add_50ms++;
    Add_100ms++;
    Add_200ms++;
    if(Add_2ms == 1)
    {
     	Add_2ms = 0;
			FLAG_2MS=1;
    }
    if(Add_5ms == 2)
    {
     	Add_5ms = 0;
			FLAG_5MS=1;
    }
    if(Add_10ms == 5)
    {
     	Add_10ms = 0;
			FLAG_10MS=1;
    }
    if(Add_50ms == 25)
    {
     	Add_50ms = 0;
			FLAG_50MS=1;
    }
    if(Add_100ms == 50)
    {
     	Add_100ms = 0;
			FLAG_100MS=1;
    }
    if(Add_200ms == 100)
    {
     	Add_200ms = 0;
			FLAG_200MS=1;
			Timer++;			//计时器加一(200ms)
    }
	  if(((Timer/5) >= Stop_Time) && (Stop_Time != 0))	//当超时且设定时间不为0时
	  {
	 	  Timer=0;
		  Error_Flag.Time_out=1;
	  }
		
	  ALL_STOP_Judge();//系统停止判断，直到错误标志位被清零跳出
		
    Balance();		//系统直立
		
		if(FLAG_5MS == 1)
		{
			FLAG_5MS=0;
			ADC_Calcu();	//电感数据接收和处理
			Camera_Calcu();	//摄像头误差计算
			Radius=AD_Data.Control_Out;		//最终误差输出值--->到转向环
	  Radius=Camera_Data.Control_Out;
	  Radius=AD_Data.Control_Out+Camera_Data.Control_Out;
		}
    OLED_P6x8Flo(0, 0, Direct_Parameter, -4);
     OLED_P6x8Flo(0, 1, Motor_Data.Speed_R, -4);
     OLED_P6x8Flo(0, 2, Motor_Data.Speed_L, -4);
     OLED_P6x8Flo(0, 3, PID_Speed.PID_Local_Out, -4);
     OLED_P6x8Flo(0, 2, PID_Ang.Param_Kd, -3);
		 **************end******/
}



/************************以下不可修改************************/
void PIN_INT7_DriverIRQHandler(void)
{
    mt9v032_vsync();        //总钻风场中断代码，当使用总钻风的时候执行该代码
}

void DMA0_DriverIRQHandler(void)
{
    if(READ_DMA_FLAG(MT9V032_DMA_CH))
    {
        CLEAR_DMA_FLAG(MT9V032_DMA_CH);
        mt9v032_dma();      //总钻风dma中断代码，当使用总钻风的时候执行该代码
    }
}




void FLEXCOMM8_DriverIRQHandler(void)
{
    vuint32 flag;
		flag = UART8_FIFO_FLAG;
	
    if(flag & USART_FIFOINTSTAT_RXLVL_MASK)//接收FIFO达到设定水平（库默认设定水平 当接收FIFO有一个数据的时候触发中断）
    {
//			uart_query(USART_8, &read_data[receive_num++]);
//			if (read_data[receive_num - 1] == 'e')
//			{
//				receive_num = 0;
//			}


      //mt9v032_cof_uart_interrupt();		//接受成功
    }
 
    if(flag & USART_FIFOINTSTAT_RXERR_MASK)//接收FIFO错误
    {
        USART8->FIFOCFG  |= USART_FIFOCFG_EMPTYRX_MASK;//清空RX FIFO
        USART8->FIFOSTAT |= USART_FIFOSTAT_RXERR_MASK;
    }
}

uint8 Flag_2ms=0, Flag_5ms=0, Flag_10ms=0 ,Flag_50ms=0 ,Flag_100ms=0 ,Flag_200ms=0;
void MRT0_DriverIRQHandler(void)
{
    if (MRT_FLAG_READ(MRT_CH0))		//1ms
    {
		MRT_FLAG_CLR(MRT_CH0);
		static uint8 Add_2ms,Add_5ms,Add_10ms,Add_50ms,Add_100ms,Add_200ms;
		Add_2ms++;
		Add_5ms++;
		Add_10ms++;
		Add_50ms++;
		Add_100ms++;
		Add_200ms++;
		
		if(Add_2ms == 2)
		{
			Add_2ms = 0;
			Flag_2ms = 1;
		}
		if(Add_5ms == 5)
		{
			Add_5ms = 0;
			Flag_5ms = 1;
		}
		if(Add_10ms == 10)
		{
			Add_10ms = 0;
			Flag_10ms = 1;
		}
		if(Add_50ms == 50)
		{
			Add_50ms = 0;
			Flag_50ms = 1;
		}
		if(Add_100ms == 50)
		{
			Add_100ms = 0;
			Flag_100ms = 1;
		}
		if(Add_200ms == 100)
		{
			Add_200ms = 0;
			Flag_200ms = 1;
		}
//		Balance();		
	}
			
    if(MRT_FLAG_READ(MRT_CH1))
    {
        MRT_FLAG_CLR(MRT_CH1);
			
    }
    
    if(MRT_FLAG_READ(MRT_CH2))
    {
        MRT_FLAG_CLR(MRT_CH2);
        
    }
    
    if(MRT_FLAG_READ(MRT_CH3))
    {
        MRT_FLAG_CLR(MRT_CH3);
        
    }
}

/*
中断函数名称，用于设置对应功能的中断函数
Sample usage:当前启用了周期定时器 通道0得中断
void RIT_DriverIRQHandler(void)
{
    ;
}
记得进入中断后清除标志位

WDT_BOD_DriverIRQHandler
DMA0_DriverIRQHandler
GINT0_DriverIRQHandler
GINT1_DriverIRQHandler
PIN_INT0_DriverIRQHandler
PIN_INT1_DriverIRQHandler
PIN_INT2_DriverIRQHandler
PIN_INT3_DriverIRQHandler
UTICK0_DriverIRQHandler
MRT0_DriverIRQHandler
CTIMER0_DriverIRQHandler
CTIMER1_DriverIRQHandler
SCT0_DriverIRQHandler
CTIMER3_DriverIRQHandler
FLEXCOMM0_DriverIRQHandler
FLEXCOMM1_DriverIRQHandler
FLEXCOMM2_DriverIRQHandler
FLEXCOMM3_DriverIRQHandler
FLEXCOMM4_DriverIRQHandler
FLEXCOMM5_DriverIRQHandler
FLEXCOMM6_DriverIRQHandler
FLEXCOMM7_DriverIRQHandler
ADC0_SEQA_DriverIRQHandler
ADC0_SEQB_DriverIRQHandler
ADC0_THCMP_DriverIRQHandler
DMIC0_DriverIRQHandler
HWVAD0_DriverIRQHandler
USB0_NEEDCLK_DriverIRQHandler
USB0_DriverIRQHandler
RTC_DriverIRQHandler
Reserved46_DriverIRQHandler
Reserved47_DriverIRQHandler
PIN_INT4_DriverIRQHandler
PIN_INT5_DriverIRQHandler
PIN_INT6_DriverIRQHandler
PIN_INT7_DriverIRQHandler
CTIMER2_DriverIRQHandler
CTIMER4_DriverIRQHandler
RIT_DriverIRQHandler
SPIFI0_DriverIRQHandler
FLEXCOMM8_DriverIRQHandler
FLEXCOMM9_DriverIRQHandler
SDIO_DriverIRQHandler
CAN0_IRQ0_DriverIRQHandler
CAN0_IRQ1_DriverIRQHandler
CAN1_IRQ0_DriverIRQHandler
CAN1_IRQ1_DriverIRQHandler
USB1_DriverIRQHandler
USB1_NEEDCLK_DriverIRQHandler
ETHERNET_DriverIRQHandler
ETHERNET_PMT_DriverIRQHandler
ETHERNET_MACLP_DriverIRQHandler
EEPROM_DriverIRQHandler
LCD_DriverIRQHandler
SHA_DriverIRQHandler
SMARTCARD0_DriverIRQHandler
SMARTCARD1_DriverIRQHandler
*/



