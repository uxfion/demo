/*****************************************************************************
 * @file        selfbuild_key.h
 * @company     ZUST Smart Car
 * @author      FION
 * @version     v1.3
 * @software
 * @core
 * @date        2019-10-13
 * @note
 ***************************************************************************/

#ifndef _SELFBUILD_KEY_H
#define _SELFBUILD_KEY_H

#include "headfile.h"

#define UP      0x01
#define DOWN    0x02
#define LEFT    0x04
#define RIGHT   0x08
#define MID     0x10

#define KEY_DELAY	25


int8 button_get(void);
void key_init(void);

 #endif
