#include "SelfBuild_Control.h"


Motor_Typedef Motor={0};
Error_Flag_Typedef Error_Flag={0};

float  gyrox,gyroy,gyroz;
float pitch,yaw,roll;
int16_t gyrox_temp=0,gyroy_temp=0,gyroz_temp=0;


uint8 init_all(void)
{
	get_clk();
	/********************data init********************/
	memset(&Motor, 0, sizeof(Motor));
	memset(&MPU6050, 0, sizeof(MPU6050));
	MPU6050.ZERO.x = 0;
	MPU6050.ZERO.y = ZERO_Pitch;	//零点角度
	MPU6050.ZERO.z = 0;
	
	/********************gpio init********************/
	gpio_init(BUZZER, GPO, OFF, NOPULL);	//init buzzer
	gpio_set(BUZZER, OFF);				//turn off buzzer
	gpio_init(A23, GPO, OFF, NOPULL);
	
	gpio_init(BUTTON_UP   , GPI, ON, PULLUP);
	gpio_init(BUTTON_DOWN , GPI, ON, PULLUP);
	gpio_init(BUTTON_LEFT , GPI, ON, PULLUP);
	gpio_init(BUTTON_RIGHT, GPI, ON, PULLUP);
	gpio_init(BUTTON_CONFIRM  , GPI, ON, PULLUP);	//init button
	gpio_init(SWITCH1_1   , GPI, ON, PULLUP);
	gpio_init(SWITCH1_2   , GPI, ON, PULLUP);
	gpio_init(SWITCH1_3   , GPI, ON, PULLUP);
	gpio_init(SWITCH1_4   , GPI, ON, PULLUP);
	
	/********************oled init********************/
	oled_init();

	
	
	/********************eeprom init********************/
	
	eeprom_init();
	/********************数字电位器 init********************/
	Correct_Sensor();
	
	
	/********************electron init********************/
	
	
	
	/********************uart init********************/
	uart_init(USART_8, 115200, UART8_TX_B18 ,UART8_RX_B17);
	uart_rx_irq(USART_8,1);
	
	
	/********************menu init********************/
	DisableInterrupts;                          //关闭所有中断，防止菜单调节过程中出现中断
	Menu_Init();                                  //初始化菜单
	while(!Menu_Work()) systick_delay_ms(200);    //菜单每200ms工作一次，并根据是否按下“关闭菜单”选项后（函数返回0）结束死循环
	EnableInterrupts;
	
	
	/********************coder init********************/
	ctimer_count_init(CODER_L);		//coder 2
	ctimer_count_init(CODER_R);
	gpio_init(CODER_DIR_L, GPI, 1,PULLUP);		//coder 2
	gpio_init(CODER_DIR_R, GPI, 1,PULLUP);
	
	/********************motor init********************/
	sct_pwm_init(PWM_CH_R_Z, 7200, 0);	
	sct_pwm_init(PWM_CH_R_F, 7200, 0);
	sct_pwm_init(PWM_CH_L_Z, 7200, 0);
	sct_pwm_init(PWM_CH_L_F, 7200, 0);
	
	
	/********************MPU6050 init*******************/
//	while(MPU6050_ADDDMPInit())systick_delay_ms(10);	//初始化DMP 
//	while(MPU6050_SelfTest());
//	oled_fill(BLACK); 
//	oled_p6x8str(0, 0, (uint8 *)"pitch=");
//	oled_p6x8str(0, 1, (uint8 *)"roll=");
//	oled_p6x8str(0, 2, (uint8 *)"yaw=");
//	while(1)
//	{
//		  DataRead();
//		  oled_float(50, 0, pitch, 3, 1);
//		  oled_float(50, 1, roll, 3, 1);
//		  oled_float(50, 2, yaw, 3, 1);
//		  if(gpio_get(BUTTON_CONFIRM) == 0)
//		  {
//			  while(!System_OK)
//			  {
//					DataRead();
//				  if(pitch<-33)
//				  System_OK=1;
//			  }
//			  oled_fill(BLACK); 
//			  break;
//		  }
//	}
	
	/******************timer init********************/
	mrt_pit_init_ms(MRT_CH0,1);
	set_irq_priority(MRT0_IRQn,0);	//set interrupt priority
	
	
	/***************init all succussfully***************/
	oled_fill(0x00);
	return SUCCESS;
}

void DataRead(void)//陀螺仪会读取数据失败
{
	static float RollReadOld = 0, PitchReadOld = 0, YawReadOld = 0;
	
	    gyroy_temp = 0;
	
	
	if(MPU6050_DMPGetData(&pitch, &roll, &yaw))
	{
		pitch = PitchReadOld;
		 roll= RollReadOld;
		 yaw = YawReadOld;
	}
	MPU6050_ReadData(MPU6050_GYRO_Y ,&gyroy_temp);
	MPU6050_ReadData(MPU6050_GYRO_Z ,&gyroz_temp);
	MPU6050_ReadData(MPU6050_GYRO_X ,&gyrox_temp);
	gyroy = (gyroy_temp) / 16.384 - 0.2259-0.3;
	gyroz = (gyroz_temp) / 16.384 - 0.2259+1-0.4;
	gyrox = (gyrox_temp) / 16.384 - 0.2259+3.4;
	PitchReadOld = pitch;
	RollReadOld = roll;
	YawReadOld = yaw;
}




uint8 PWM_Set(void)		//pwm output
{
//	Motor.PWM_L_Set += Dead_PWM;	//去除死区PWM
//	Motor.PWM_R_Set += Dead_PWM;
//	Motor.PWM_L_Set=RANGE(Motor.PWM_L_Set,PWM_MAX,-PWM_MAX);		//PWM限幅
//	Motor.PWM_R_Set=RANGE(Motor.PWM_R_Set,PWM_MAX,-PWM_MAX);
	sct_pwm_duty(PWM_CH_L_Z,((Motor.PWM_L_Set>0)?Motor.PWM_L_Set:0));	//PWM输出
	sct_pwm_duty(PWM_CH_L_F,((Motor.PWM_L_Set<0)?(-Motor.PWM_L_Set):0)); 
	sct_pwm_duty(PWM_CH_R_Z,((Motor.PWM_R_Set>0)?Motor.PWM_R_Set:0));
	sct_pwm_duty(PWM_CH_R_F,((Motor.PWM_R_Set<0)?(-Motor.PWM_R_Set):0));
	
	return SUCCESS;
}




uint8 Speed_Get(void)	//得到电机速度（编码器）
{
	Motor.Speed_L = (gpio_get(CODER_DIR_L) == 1) ? ctimer_count_read(CODER_L) : -ctimer_count_read(CODER_L);
	Motor.Speed_R = (gpio_get(CODER_DIR_R) == 0) ? ctimer_count_read(CODER_R) : -ctimer_count_read(CODER_R);
	ctimer_count_clean(CODER_L);
	ctimer_count_clean(CODER_R);
	
	
	
	

//	Motor.Right_Acc = Motor.Speed_R - Motor.Speed_R_Last;	// 计算加速度
//	if (Motor.Right_Acc  > 100)
//	{
//		Motor.Right_Crazy = 1;	// 疯转
//	}
//	if (Motor.Speed_R > Motor.Speed_Ave_Set + 200)
//	{
//		Motor.Right_Crazy = 2;	// 疯转
//	}
//	if (Motor.Speed_R < -350)
//	{
//		Motor.Right_Crazy = -1;	// 倒转
//	}

//	if (Motor.Right_Crazy)
//	{
//		if (Motor.Right_Acc  <= 100)
//		{
//			if (Motor.Speed_R < Motor.Speed_Ave_Set + 200 && Motor.Speed_R > 0)
//			{
//				Motor.Right_Crazy = 0;
//			}
//		}
//	}

//	if (!Motor.Right_Crazy)
//	{
//		Motor.Speed_R = Motor.Speed_R*0.9 + Motor.Speed_R_Last*0.1;
//		Motor.Speed_R_Last = Motor.Speed_R;	// 更新右轮速度
//	}
//	else
//	{
//		Motor.Speed_R = Motor.Speed_R*0.5 + Motor.Speed_R_Last*0.5;
//		Motor.Speed_R_Last = Motor.Speed_R;	// 更新右轮速度
//	}
//	/******* 右电机速度相关控制结束 ********/

//	/******* 左电机速度相关控制 ********/

//	Motor.Left_Acc = Motor.Speed_L - Motor.Speed_L_Last;	// 计算加速度
//	if (Motor.Left_Acc > 100)
//	{
//		Motor.Left_Crazy = 1;
//	}
//	if (Motor.Speed_L > Motor.Speed_Ave_Set + 200)
//	{
//		Motor.Left_Crazy = 2;
//	}
//	if (Motor.Speed_L < -350)
//	{
//		Motor.Left_Crazy = -1;
//	}

//	if (Motor.Left_Crazy)
//	{
//		if (Motor.Left_Acc <= 100)
//		{
//			if (Motor.Speed_L < Motor.Speed_Ave_Set + 200 && Motor.Speed_L > 0)
//			{
//				Motor.Left_Crazy = 0;
//			}
//		}
//	}

//	if (!Motor.Left_Crazy)
//	{
//		Motor.Speed_L = 0.9*Motor.Speed_L + 0.1*Motor.Speed_L_Last;	// 低通滤波
//		Motor.Speed_L_Last = Motor.Speed_L;	// 更新左轮速度
//	}
//	else
//	{
//		Motor.Speed_L = 0.5*Motor.Speed_L + 0.5*Motor.Speed_L_Last;	// 低通滤波
//		Motor.Speed_L_Last = Motor.Speed_L;	// 更新左轮速度
//	}



//	/******* 左电机速度相关控制结束 */

//	if ((Motor.Left_Crazy && Motor.Right_Crazy) || (Motor.Left_Crazy && Motor.Speed_R < 20) || (Motor.Right_Crazy && Motor.Speed_L < 20))
//	{
//		Motor.Crazy_Count++;
//		if (Motor.Crazy_Count >= 40)
//		{
//			Motor.Crazy_Count 	= 0;
//			Motor.Crazy_StopFlag = 1;
//		}
//	}
//	else
//	{
//		Motor.Right_Crazy = 0;
//	}

//	/******* 电机疯转特殊处理 ********/
//	if ((Motor.Left_Crazy > 0) && (Motor.Right_Crazy > 0))
//	{
//		Motor.Speed_Ave_Now = Motor.Speed_Ave_Set;			// 两边都疯转，使用上次速度作为当前实际速度
//	}
//	else if (Motor.Left_Crazy)
//	{
//		if (Motor.Speed_R > Motor.Speed_Ave_Set)
//		{
//			Motor.Speed_Ave_Now = Motor.Speed_Ave_Set;
//		}
//		else
//		{
//			Motor.Speed_Ave_Now = Motor.Speed_R;	// 左电机疯转，使用上次速度作为当前实际速度
//		}
//	}
//	else if (Motor.Right_Crazy)
//	{
//		if (Motor.Speed_L > Motor.Speed_Ave_Set)
//		{
//			Motor.Speed_Ave_Now = Motor.Speed_Ave_Set;
//		}
//		else
//		{
//			Motor.Speed_Ave_Now = Motor.Speed_L;	// 右电机疯转，使用上次速度作为当前实际速度
//		}
//	}
//	else
//	{
//		Motor.Speed_Ave_Now = (Motor.Speed_L + Motor.Speed_R) / 2;	// 左右取平均计算车子实际速度
//	}

//	Motor.Speed_Ave_Now = Motor.Speed_Ave_Now *0.9 + Motor.Speed_Ave_Last * 0.1;
//	Motor.Speed_Ave_Last = Motor.Speed_Ave_Now;
	return SUCCESS;
}








