/*!
   两环(角度环(外环）,角速度环(内环）)控制小车平衡，速度环控制车速，转向环转向（阿克曼差速）
 * @author    FYJ
 * @version    v5.0
 * @date       2013-08-28
 */



#include "common.h"

#include "SelfBuild_balance.h"
#include "math.h"


#define 	ZL_Zero_Angle 4400 //机械零点
#define 	SL_Zero_Angle	800

void PWMOut(void);
void Car_sanlun(void);
float UI_data[8];
float Speed_Set=0;
float Speed_Now=0;
float Speed_Min=0;

char Left_Crazy = 0;	// 电机疯转
char Right_Crazy = 0;	// 电机疯转
int16 g_nLeftPWM, g_nRighPWM;					//左右轮PWM输出值
								
int16 Left_duty = 0;
int16 Right_duty = 0; 
int16 g_nSpeedLeft = 0; 
int16 g_nSpeedRight = 0;

int16 Tar_Ang_Vel_Y=0;//角速度
int16 Target_Angle_Y=0;//角加速度
int16 Radius=0;
int16 Direct_Parameter=0;//转向环
int16 Direct_Last=0;//上次角速度


uint16 g_nBattery=0;//电池

int16 MOTOR_Speed_Left = 0;
int16 MOTOR_Speed_Right = 0; 
int16 MOTOR_Speed_Left_Last = 0;//上一次左轮速度;
int16 MOTOR_Speed_Right_Last = 0;//上一次右轮速度
int16 MOTOR_Left_Acc = 0;
int16 MOTOR_Right_Acc = 0;


char Speed_Flag, Angle_Flag, Ang_Velocity_Flag;

//---------------MPU6050----------------------
float g_fGyroYRead;
float g_fGyroZRead;
float g_fGyroXRead;
float pitch = 0, roll = 0, yaw = 0;//欧拉角
int16 fGyroY=0;//Y轴角速度
int16 fGyroZ=0;//Z轴角速度
int16 fGyroX=0;//x轴角速度
void DataRead(void)//陀螺仪会读取数据失败
{
	static float RollReadOld = 0, PitchReadOld = 0, YawReadOld = 0;
	int16 fGyroY = 0;
	
 //g_nBattery =(uint16 )(adc_get_once(AD_CH_BAT,ADC_12bit) / 4095.0 * 2.95* 3.3 * 100);
	
	if(MPU6050_DMPGetData(&pitch, &roll, &yaw))
	{
	   pitch = PitchReadOld;
		 roll= RollReadOld;
		 yaw = YawReadOld;
	}
	MPU6050_ReadData(MPU6050_GYRO_Y ,&fGyroY);
	MPU6050_ReadData(MPU6050_GYRO_Z ,&fGyroZ);
	MPU6050_ReadData(MPU6050_GYRO_X ,&fGyroX);
	g_fGyroYRead = (fGyroY) / 16.384 - 0.2259;
	g_fGyroZRead = (fGyroZ) / 16.384 - 0.2259+1;
	g_fGyroXRead = (fGyroX) / 16.384 - 0.2259;
	PitchReadOld = pitch;
	RollReadOld = roll;
	YawReadOld = yaw;
}

 
/******* 电机速度测量 ********/
void Speed_Measure(void)
{
	static int16 Speed_Last = 0;
	static int16 Crazy_Count = 0;
	

	/******* 右电机速度相关控制 ********/
	g_nSpeedRight =(gpio_get(CODER_DIR_R) == 0) ? ctimer_count_read(CODER_R) : -ctimer_count_read(CODER_R);
	g_nSpeedRight *= 0.5;
	ctimer_count_clean(CODER_R); 
	MOTOR_Speed_Right =g_nSpeedRight;
	MOTOR_Right_Acc = MOTOR_Speed_Right - MOTOR_Speed_Right_Last;	// 计算加速度
	if (MOTOR_Right_Acc > 100)
	{
		Right_Crazy = 1;	// 疯转
	}
	if (MOTOR_Speed_Right > Speed_Set + 200)
	{
		Right_Crazy = 2;	// 疯转
	}
	if (MOTOR_Speed_Right < -350)
	{
		Right_Crazy = -1;	// 倒转
	}
	
	if (Right_Crazy)
	{
		if (MOTOR_Right_Acc <= 100)
		{
			if (MOTOR_Speed_Right < Speed_Set + 200 && MOTOR_Speed_Right > 0)
			{
				Right_Crazy = 0;
			}
		}
	}
	
	if (!Right_Crazy)
	{
		MOTOR_Speed_Right = MOTOR_Speed_Right*0.9 + MOTOR_Speed_Right_Last*0.1;
		MOTOR_Speed_Right_Last = MOTOR_Speed_Right;	// 更新右轮速度
	}
	else
	{
		MOTOR_Speed_Right = MOTOR_Speed_Right*0.5 + MOTOR_Speed_Right_Last*0.5;
		MOTOR_Speed_Right_Last = MOTOR_Speed_Right;	// 更新右轮速度
	}
	/******* 右电机速度相关控制结束 ********/
	
	/******* 左电机速度相关控制 ********/
	g_nSpeedLeft =(gpio_get(CODER_DIR_L) == 1) ?ctimer_count_read(CODER_L) : -ctimer_count_read(CODER_L);//获取转速
	ctimer_count_clean(CODER_L);

	MOTOR_Speed_Left = g_nSpeedLeft;		// 得到左轮转速
	
	MOTOR_Left_Acc = MOTOR_Speed_Left - MOTOR_Speed_Left_Last;	// 计算加速度
	if (MOTOR_Left_Acc > 100)
	{
		Left_Crazy = 1;
	}
	if (MOTOR_Speed_Left > Speed_Set + 200)
	{
		Left_Crazy = 2;
	}
	if (MOTOR_Speed_Left < -350)
	{
		Left_Crazy = -1;
	}
	
	if (Left_Crazy)
	{
		if (MOTOR_Left_Acc <= 100)
		{
			if (MOTOR_Speed_Left < Speed_Set + 200 && MOTOR_Speed_Left > 0)
			{
				Left_Crazy = 0;
			}
		}
	}
	
	if (!Left_Crazy)
	{
		MOTOR_Speed_Left = 0.9*MOTOR_Speed_Left + 0.1*MOTOR_Speed_Left_Last;	// 低通滤波
		MOTOR_Speed_Left_Last = MOTOR_Speed_Left;	// 更新左轮速度
	}
	else
	{
		MOTOR_Speed_Left = 0.5*MOTOR_Speed_Left + 0.5*MOTOR_Speed_Left_Last;	// 低通滤波
		MOTOR_Speed_Left_Last = MOTOR_Speed_Left;	// 更新左轮速度
	}

	
	
	/******* 左电机速度相关控制结束 ********/
	
	
	if ((Left_Crazy && Right_Crazy) || (Left_Crazy && MOTOR_Speed_Right < 20) || (Right_Crazy && MOTOR_Speed_Left < 20))
	{
		Crazy_Count++;
		if (Crazy_Count >= 40)
		{
			Crazy_Count = 0;
		}
	}
	else
	{
		Right_Crazy = 0;
	}
	
	/******* 电机疯转特殊处理 ********/
	if ((Left_Crazy > 0) && (Right_Crazy > 0))
	{
		Speed_Now = Speed_Set;			// 两边都疯转，使用上次速度作为当前实际速度
	}
	else if (Left_Crazy)
	{
		if (MOTOR_Speed_Right > Speed_Set)
		{
			Speed_Now = Speed_Set;
		}
		else
		{
			Speed_Now = MOTOR_Speed_Right;	// 左电机疯转，使用上次速度作为当前实际速度
		}
	}
	else if (Right_Crazy)
	{
		if (MOTOR_Speed_Left > Speed_Set)
		{
			Speed_Now = Speed_Set;
		}
		else
		{
			Speed_Now = MOTOR_Speed_Left;	// 右电机疯转，使用上次速度作为当前实际速度
		}
	}
	else
	{
		Speed_Now = (MOTOR_Speed_Left + MOTOR_Speed_Right) / 2;	// 左右取平均计算车子实际速度
	}
	
	Speed_Now = Speed_Now *0.9f + Speed_Last * 0.1f;
	Speed_Last = Speed_Now;
	
}


/*************************直立模式************************/ 
// 角速度环2ms一次 角度环10ms一次  速度环100ms一次 
// 转向内环2ms一次  转向外环10ms一次
void Car_balance(void)
{

  if(Flag_zitai==0) 
  {
		ZL_SPEED_PID.MAX = 2000;
		ZL_SPEED_PID.MIN = -2000;
		Speed_control();
		if(Ang_Velocity_Flag)
   {
       Ang_Velocity_Flag=0;
			 DataRead();
       Right_duty =(int16)PID_Incremental(&ZL_Ang_Velocity_PID, g_fGyroYRead*10,Tar_Ang_Vel_Y); //角速度环Tar_Ang_Vel_Y
	     Left_duty =(int16)PID_Incremental(&ZL_Ang_Velocity_PID, g_fGyroYRead*10,Tar_Ang_Vel_Y);//Tar_Ang_Vel_Y
       if(Flag_Run)
			 { 
				// Speed_Set = Speed_zhili;
							/* 角速度环作为最内环控制转向 */	
					 Direct_Parameter = PID_Positional(&ZL_Z_PID,g_fGyroZRead,(Radius*Speed_Min)/10);	// 转向环左正右负
					 Direct_Last = Direct_Last*0.2 + Direct_Parameter*0.8;	// 更新上次角速度环结果	 

						Left_duty   = Left_duty  - Direct_Last;	// 左右电机根据转向系数调整差速
						Right_duty  = Right_duty  + Direct_Last;	
			 }				 
			 PWMOut(); 		 
   }
   if(Angle_Flag)
   {
	      Angle_Flag=0;
        Speed_Measure(); //车速测量
        Tar_Ang_Vel_Y=PID_Positional(&ZL_Angle_PID,pitch*100,Target_Angle_Y) ;	//角度环Target_Angle_Y 
   }
   if(Speed_Flag)
   {  
    Speed_Flag = 0;
    if(Speed_Now<40)
			Speed_Min=40;
		else
			Speed_Min=Speed_Now;
    Target_Angle_Y = ZL_Zero_Angle-(int16)PID_Positional(&ZL_SPEED_PID,Speed_Now,Speed_Set);  //速度环
   }
  }

}




/**********************三轮模式************************/
//速度环10ms一次  转向内环 2ms  转向外环10ms

void Car_sanlun(void)
{
	if(Flag_zitai==1) 	
  {					 
		Speed_control();
		if(Hengduan_flag == 0)
  	{
		  if(Ang_Velocity_Flag)
     {
       Ang_Velocity_Flag=0;
			 DataRead();
			 Direct_Parameter = PID_Positional(&SL_Z_PID,g_fGyroZRead,(Radius*Speed_Min)/10);	// 转向环左正右负
		   Direct_Last = Direct_Last*0.2 + Direct_Parameter*0.8;	// 更新上次角速度环结果	 
		 } 
    if(Flag_Run)
		 {
				//Speed_Set = Speed_sanlun;
			  Left_duty   = Left_duty   - Direct_Last;	// 左右电机根据转向系数调整差速
			  Right_duty  = Right_duty  + Direct_Last;
			 	PWMOut(); 
     }
			 			/* 角速度环作为最内环控制转向 */		 
		 if(Angle_Flag)
		 {   
				Angle_Flag = 0;
				 Speed_Measure();
				if(Speed_Now<40)
					Speed_Min=40;
				else
					Speed_Min=Speed_Now;
				if(pitch>10.00f)//限制三轮抬轮但影响车速，放开抬轮角度
				{
				Right_duty =(int16)PID_Incremental(&SL_SPEED_PID,Speed_Now,Speed_Set)+PID_Positional(&SL_Angle_PID,pitch*100,300);  //速度环
				Left_duty =(int16)PID_Incremental(&SL_SPEED_PID,Speed_Now,Speed_Set)+PID_Positional(&SL_Angle_PID,pitch*100,300);
				}
				else
				{
				Right_duty =(int16)PID_Incremental(&SL_SPEED_PID,Speed_Now,Speed_Set);//速度环
				Left_duty =(int16)PID_Incremental(&SL_SPEED_PID,Speed_Now,Speed_Set);
				}
			}
		}
		else 
		{
			ZL_SPEED_PID.MAX = 50;
			ZL_SPEED_PID.MIN = -100;
			if(Ang_Velocity_Flag)
		 {
				 Ang_Velocity_Flag=0;
				 DataRead();
				 Right_duty =(int16)PID_Incremental(&ZL_Ang_Velocity_PID, g_fGyroYRead*10,Tar_Ang_Vel_Y); //角速度环Tar_Ang_Vel_Y
				 Left_duty =(int16)PID_Incremental(&ZL_Ang_Velocity_PID, g_fGyroYRead*10,Tar_Ang_Vel_Y);//Tar_Ang_Vel_Y
				 if(Flag_Run)
				 {
					 //Speed_Set = 110;

								/* 角速度环作为最内环控制转向 */	
					 Direct_Parameter = PID_Positional(&ZL_Z_PID,g_fGyroZRead,(Radius*Speed_Min)/10);	// 转向环左正右负
					 Direct_Last = Direct_Last*0.2 + Direct_Parameter*0.8;	// 更新上次角速度环结果	 

						Left_duty   = Left_duty  - Direct_Last;	// 左右电机根据转向系数调整差速
						Right_duty  = Right_duty  + Direct_Last;	
				 }				 
				 PWMOut(); 		 
		 }
		 if(Angle_Flag)
		 {
					Angle_Flag=0;
					Speed_Measure(); //车速测量
					Tar_Ang_Vel_Y=PID_Positional(&ZL_Angle_PID,pitch*100,Target_Angle_Y) ;	//角度环Target_Angle_Y 
		 }
		 if(Speed_Flag)
		 {  
			Speed_Flag = 0;
			if(Speed_Now<40)
				Speed_Min=40;
			else
				Speed_Min=Speed_Now;
			Target_Angle_Y = SL_Zero_Angle-(int16)PID_Positional(&ZL_SPEED_PID,Speed_Now,Speed_Set);  //速度环
		 }
		}
  }
}
//**********************PWM输出**********************
/**
 * @备注    PWM输出函数
 * @输出    g_nLeftPWM								左轮输出PWM值
						g_nRightPWM								右轮输出PWM值
 */
void PWMOut(void)
{
	g_nLeftPWM = Left_duty;
	g_nRighPWM = Right_duty;
	
if(Flag_Run)
	{
		if(g_nRighPWM >= 0)
		{
			if(g_nRighPWM + PWM_DEAD_R_Z >= PWM_MAX) g_nRighPWM = PWM_MAX - PWM_DEAD_R_Z;
			if(Flag_Stop == ON) g_nRighPWM -= PWM_DEAD_R_Z;
			sct_pwm_duty(PWM_CH_R_F,0);
			sct_pwm_duty(PWM_CH_R_Z,g_nRighPWM + PWM_DEAD_R_Z);
		}
		else if(g_nRighPWM < 0)
		{
			if(g_nRighPWM - PWM_DEAD_R_F <= PWM_MIN) g_nRighPWM = PWM_MIN + PWM_DEAD_R_F;
			if(Flag_Stop == ON) g_nRighPWM += PWM_DEAD_R_Z;
			sct_pwm_duty(PWM_CH_R_F,- g_nRighPWM + PWM_DEAD_R_F);
			sct_pwm_duty(PWM_CH_R_Z,0);
		}
		
		if(g_nLeftPWM >= 0)
		{
			if(g_nLeftPWM + PWM_DEAD_L_Z >= PWM_MAX) g_nLeftPWM = PWM_MAX - PWM_DEAD_L_Z;
			if(Flag_Stop == ON) g_nRighPWM -= PWM_DEAD_L_Z;
			sct_pwm_duty(PWM_CH_L_F,0);
			sct_pwm_duty(PWM_CH_L_Z,g_nLeftPWM + PWM_DEAD_L_Z);
		}
		else if(g_nLeftPWM < 0)
		{
			if(g_nLeftPWM - PWM_DEAD_L_F <= PWM_MIN) g_nLeftPWM = PWM_MIN + PWM_DEAD_L_F;
			if(Flag_Stop == ON) g_nRighPWM -= PWM_DEAD_L_F;
			sct_pwm_duty(PWM_CH_L_F,- g_nLeftPWM + PWM_DEAD_L_F);
			sct_pwm_duty(PWM_CH_L_Z,0);
		}
	}
	else
	{
			if(Speed_Now>10)
			{
			sct_pwm_duty(PWM_CH_L_Z, 0);
			sct_pwm_duty(PWM_CH_L_F, 800);
			sct_pwm_duty(PWM_CH_R_Z, 0);	
			sct_pwm_duty(PWM_CH_R_F, 800);	
			}
			else
			{
			sct_pwm_duty(PWM_CH_L_Z, 0);
			sct_pwm_duty(PWM_CH_L_F, 0);
			sct_pwm_duty(PWM_CH_R_Z, 0);	
			sct_pwm_duty(PWM_CH_R_F, 0);	
			}	
		
	}
}
