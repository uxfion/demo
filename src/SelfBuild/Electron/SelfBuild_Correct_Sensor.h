#ifndef _SELFBUILD_CORRECT_SENSOR_H
#define _SELFBUILD_CORRECT_SENSOR_H

#include "headfile.h"

#define	FLASH_SAVE_MCP41				(EEPROM_PAGE_COUNT - 23)

#define ADC_R_R_Min_8bit 100	//五电感最小值
#define ADC_R_Min_8bit   100
#define ADC_M_Min_8bit   100
#define ADC_L_Min_8bit   100
#define ADC_L_L_Min_8bit 100

void EEPROM_InitMCP41(void);
void Correct_Sensor(void);
void Normalized_MCP41(void);


#endif
