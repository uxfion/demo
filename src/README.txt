# LPC54606




## version list

2019.11.14	创建工程 -chm
			Project 里可以同时打开IAR工程与MDK工程
			**工程倒闭** 改用原工程
			
2019.11.16	SelfBuild_Control 电机、编码器端口匹配

2019.11.20	SelfBuild_MPU6050 陀螺仪读值

2019.11.22	SelfBuild_Balance 平衡控制 **失败**

2019.11.23  OLEDMenu  完成
				修改 SEEKFREE_OLED.h 增加OLED函数兼容以匹配OLEDMenu
				增加 LPC546XX_eeprom 下的 my_eeprom_write_word()；
				增加 SelfBuild_OLEDMenu
			
2019.11.23	增加 Elecron 文件夹
				包括 MCP41、SelfBuild_Correct_Sensor、SelfBuild_Electron_Calcu

2019.11.24  Correct_Sensor 完成
			Electron_Calcu 更新
			欲改用费英杰的PID，严一展的看着脑瓜疼
			转向裂开
			
2019.11.25  解决 Correct_Sensor 中无法保存电阻值至 eeprom
			原因：初始化两次 eeprom
			改电感读到的最大值至100
			Diff_Ratio 差比和计算正常
			Radius	转弯半径倒数传出正常
			转向外环参数待定
			
2019.11.26		重新更新Balance，调参数
Balance_v1.2	Balance 完成，Ang_Velo、Ang 初步完成
				加速度环
				菜单初始化不了
			
2019.11.27		菜单初始化  解决
Balance_v1.3	原因：变量超过16个
				速度环仍未解决  给DSJ
Balance_v1.4	开始调转向内环
Electron_v1.2	初步转弯
Electron_v1.3	调电磁参数
				未移植DSJ代码
Balance_v1.6	移植DSJ代码
				初步成功
				
2019.11.29		继续移植DSJ代码
Balance_v1.7	改变量成功
				平衡有问题，会出现倒退，原因未知
				转向环会逐步产生震荡
				FYJ：前瞻太重，尾部加配重
		
			
			
## PID Para
***********DSJ*******************
PID PID_Ang_Velo={
		2,
		0.08,
		0,
		990,
		-990,
};
**********************************


**********HAOMING CHEN************

PID PID_Ang_Velo={
		2.1,
		0.08,
		0,
		990,
		-990,
};

PID PID_Ang={
          1.1,
          0,
          2.90,
          1000,
          -1000,  
};

*********************************

**********YYZ********************

PID PID_Ang_Velo={
		2.1,
		0.08,
		0,
		990,
		-990,
};

PID PID_Ang={
          1.1,
          0,
          3.20,
          1000,
          -1000,  
};
*****************************

			
			
