/*****************************************************************************
 * @file        selfbuild_key.c
 * @company     ZUST Smart Car
 * @author      FION
 * @version     v1.3
 * @software
 * @core
 * @date        2019-10-13
 * @note
 ***************************************************************************/

#include "SelfBuild_Key.h"

//---------------------------------------------------------------------------
//  @brief        按键扫描
//  @param
//  @return       key_value   键值
//  @notice       按下按键，蜂鸣器数毫秒；松开按键，执行程序
//  @issue        times 按键防抖时间不固定
//  @usage        button_get();
//---------------------------------------------------------------------------
int8 button_get(void)
{

  static int8 times = 0;
  static int8 flag_up = 0, flag_down = 0, flag_left = 0, flag_right = 0, flag_mid = 0, flag_buzzer = 0;
  int8 key_value = 0;
  times ++;
  if(times >= KEY_DELAY)   //每 times ms执行一次
		{
		  times = 0;
		    if(gpio_get(BUTTON_UP) == 0 && flag_up == 0) 					//button_up
			  flag_up = 1, flag_buzzer = 5;
			else if(gpio_get(BUTTON_UP) == 1 && flag_up == 1)				//判断放手
				flag_up = 0, key_value |= UP;

			if(gpio_get(BUTTON_DOWN) == 0 && flag_down == 0) 				//button_down
				flag_down = 1, flag_buzzer = 5;
			else if(gpio_get(BUTTON_DOWN) == 1 && flag_down == 1)		//判断放手
				flag_down = 0, key_value |= DOWN;

			if(gpio_get(BUTTON_LEFT) == 0 && flag_left == 0) 				//button_left
				flag_left = 1, flag_buzzer = 5;
			else if(gpio_get(BUTTON_LEFT) == 1 && flag_left == 1)		//判断放手
				flag_left = 0, key_value |= LEFT;

			if(gpio_get(BUTTON_RIGHT) == 0 && flag_right == 0) 			//button_right
				flag_right = 1, flag_buzzer = 5;
			else if(gpio_get(BUTTON_RIGHT) == 1 && flag_right == 1)	//判断放手
				flag_right = 0, key_value |= RIGHT;

			if(gpio_get(BUTTON_MID) == 0 && flag_mid == 0)					//button_mid
				flag_mid = 1, flag_buzzer = 5;
			else if(gpio_get(BUTTON_RIGHT) == 1 && flag_mid == 1)		//判断放手
				flag_mid = 0, key_value |= MID;
	 }
   if(flag_buzzer > 0)
		 gpio_set(BUZZER, 1), flag_buzzer--;											//buzzer
    else gpio_set(BUZZER, 0);

	return key_value;
 }

