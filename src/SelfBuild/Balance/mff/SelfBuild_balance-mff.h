#ifndef _SELFBUILD_BALANCE_H_
#define _SELFBUILD_BALANCE_H_

#include "headfile.h"

extern float Turn[5][4];
					          
extern char Speed_Flag, Angle_Flag, Ang_Velocity_Flag,Run_Flag;
extern int16 Left_duty ;
extern int16 Right_duty ; 
extern int16 g_nSpeedLeft ; 
extern int16 g_nSpeedRight;
extern float g_fGyroZRead;
extern float g_fGyroYRead;
extern float g_fGyroXRead;
extern int16 fGyroX;
extern int16 fGyroY;
extern int16 fGyroZ;
void DataRead(void);

extern int16 Tar_Ang_Vel_Y;//目标角速度
extern int16 Target_Angle_Y;//目标角度
extern int16 Radius;//转弯半径
extern float pitch,roll,yaw;
extern uint16 g_nBattery;
extern float Speed_Now;
extern float Speed_Set;
extern float MidLineNow;
extern float MdLineOld;
extern float MidLineNow;
extern char Speed_Flag, Angle_Flag, Ang_Velocity_Flag;
extern float  Speed_Min;
void Car_balance(void);
void Car_sanlun(void);
void duanlu_check(void);

#define	PWM_DEAD_R_Z						0  //8
#define	PWM_DEAD_R_F						0 //8
#define	PWM_DEAD_L_Z						0  //15
#define	PWM_DEAD_L_F						0  //15
#endif
