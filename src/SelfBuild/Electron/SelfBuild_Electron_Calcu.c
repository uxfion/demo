#include "SelfBuild_Electron_Calcu.h"



struct AD_Data_Typedef  AD_Data;

uint8 ADC_DataGet(void)	
{
	uint16 ad_temp[5][5]={0};
	
	for(int8 i=0;i<5;i++)	//取五次电感值
	{
		ad_temp[0][i]=adc_convert(AD_CH_R__, ADC_12BIT);
		ad_temp[1][i]=adc_convert(AD_CH_R_1, ADC_12BIT);
		ad_temp[2][i]=adc_convert(AD_CH_M_1, ADC_12BIT);
		ad_temp[3][i]=adc_convert(AD_CH_L_1, ADC_12BIT);
		ad_temp[4][i]=adc_convert(AD_CH_L__, ADC_12BIT);
	}
	for(int8 i=0;i<5;i++)//冒泡排序升序五次电感值
	{
		for(int8 j=0;j<4;j++)
		{
			for(int8 k=0;k<4-j;k++)
			{
				if(ad_temp[i][k] > ad_temp[i][k+1])        //前面的比后面的大  则进行交换
				{
					uint16 temp;
					temp = ad_temp[i][k+1];
					ad_temp[i][k+1] = ad_temp[i][k];
					ad_temp[i][k] = temp;
				}
			}
		}
	}
	//电感值归一化，排序后中间三个值得平均值作为当前电感值
    AD_Data.AD_R_R = (uint8)RANGE(100-(100*(((ad_temp[0][1] + ad_temp[0][2] + ad_temp[0][3]) / 3.0-ADC_R_R_Min)/ ((ADC_Max-ADC_R_R_Min)*1.0))),155,0);
	AD_Data.AD_R   = (uint8)RANGE(100-(100*(((ad_temp[1][1] + ad_temp[1][2] + ad_temp[1][3]) / 3.0-ADC_R_Min)  / ((ADC_Max-ADC_R_Min)  *1.0))),155,0);
    AD_Data.AD_M   = (uint8)RANGE(100-(100*(((ad_temp[2][1] + ad_temp[2][2] + ad_temp[2][3]) / 3.0-ADC_M_Min)  / ((ADC_Max-ADC_M_Min)  *1.0))),155,0);
    AD_Data.AD_L   = (uint8)RANGE(100-(100*(((ad_temp[3][1] + ad_temp[3][2] + ad_temp[3][3]) / 3.0-ADC_L_Min)  / ((ADC_Max-ADC_L_Min)  *1.0))),155,0);
    AD_Data.AD_L_L = (uint8)RANGE(100-(100*(((ad_temp[4][1] + ad_temp[4][2] + ad_temp[4][3]) / 3.0-ADC_L_L_Min)/ ((ADC_Max-ADC_L_L_Min)*1.0))),155,0);


    return SUCCESS;
}

uint8 ADC_LastDataSave(void)	//保存上次AD值函数
{
    AD_Data.AD_L_Last   =  AD_Data.AD_L;
    AD_Data.AD_R_Last   =  AD_Data.AD_R;
    AD_Data.AD_L_L_Last =  AD_Data.AD_L_L;
    AD_Data.AD_R_R_Last =  AD_Data.AD_R_R;
    AD_Data.AD_M_Last   =  AD_Data.AD_M;
    return SUCCESS;	
}

uint8 ADC_Calcu(void)	//10ms
{
	static float Diff_Ratio = 0, Differ = 0;
	
	ADC_DataGet();	//电感数据读取并滤波
	

	Differ = AD_Data.AD_R_R - AD_Data.AD_L_L;
	Diff_Ratio = 100 * (float)(Differ) / (float)(AD_Data.AD_L_L + AD_Data.AD_R_R + AD_Data.AD_M);
	
	
	AD_Data.Differ = Differ;
	AD_Data.Diff_Ratio = Diff_Ratio;
	
	Radius = PID_Positional(&PID_Elec, AD_Data.Diff_Ratio, 0);
	
	return SUCCESS;
	
}
	
	


/**********************************************************
int8 Flag_Huandao=0;				//环岛标志
float GYROZ_SUM=0;
float fHuandaoErrorOut = 0;
int8 Flag_Huandao_DIR=0;

//环岛参数调整
float g_fHuandaoTurn =0.8;
int16 g_nHuandaoAnger = 330;//115
int16 Huandao_DIR[3] = {0,0,0};//1左环 -1右环
double Hengduan_angle = 0;
uint8 ADC_Calcu(void)	//电感算法
{
  static float chabihe=0,CHA=0,g_fDirectionError=0,g_fHengduan_Error=0;
 
  static int Huandao_Timer=0;					
	static int16	 Huandao_Rise=0;				//中间树电感下降沿计数
  static int16 ADC_Prodata_M_1[3] = {0};
	static int8 Huandao_time;	
	
	if(gpio_get(Switch1_  1)==1)
	{
		Hengduan_angle= Hengduan_Podao_judge();
	}
		ADC_DataGet();	//电感数据读取并滤波;
	if(AD_Data.AD_M> 8 )	//锁差
	{
		CHA = AD_Data.AD_R_R - AD_Data.AD_L_L;
	}
	chabihe = 100 * (float)(CHA) / (float)(AD_Data.AD_L_L + AD_Data.AD_R_R+AD_Data.AD_M);

if(Flag_zitai==0)//直立
{
	if(Flag_Huandao == 0 && ((AD_Data.AD_R_R> 45&& AD_Data.AD_L_L>25) || (AD_Data.AD_L_L >45&& AD_Data.AD_R_R>25) )&&AD_Data.AD_M>65 )//准备进环岛
	{
		Flag_Huandao = 1;
		Flag_Huandao_DIR = Huandao_DIR[Huandao_time];
	}	
}
else if(Flag_zitai==1)//三轮
{
	if(Flag_Huandao == 0 && ((AD_Data.AD_R_R> 60&& AD_Data.AD_L_L>25) || (AD_Data.AD_L_L >60&& AD_Data.AD_R_R>25) )&&AD_Data.AD_M>120 )//准备进环岛
	{
		Flag_Huandao = 1;
		Flag_Huandao_DIR = Huandao_DIR[Huandao_time];
	}	
}
	if(Flag_Huandao == 1 || Flag_Huandao == 3 || Flag_Huandao == 5 )		//若进入环岛，开始记录旋转角度
	{
		GYROZ_SUM += (g_fGyroZRead*0.935f * 0.01f * 1.125f);
	  Huandao_Timer++;
	}
  
	if(Flag_Huandao == 1)
	{
		if(Huandao_Rise == 0 &&(((int)(AD_Data.AD_M ) > ADC_Prodata_M_1[0] && ADC_Prodata_M_1[0] > ADC_Prodata_M_1[1] && ADC_Prodata_M_1[1] > ADC_Prodata_M_1[2])))
		{
			Huandao_Rise = 3;
		}
		if(Huandao_Rise == 3 && (int)(AD_Data.AD_M) < ADC_Prodata_M_1[0])//2
		{
			Flag_Huandao = 3;
			ADC_Prodata_M_1[0] = 0, ADC_Prodata_M_1[1] = 0, ADC_Prodata_M_1[2] = 0;
		}
		ADC_Prodata_M_1[2] = ADC_Prodata_M_1[1], ADC_Prodata_M_1[1] = ADC_Prodata_M_1[0], ADC_Prodata_M_1[0] = (int)(AD_Data.AD_M);
	}
	if(Flag_Huandao == 3)
	{	
		fHuandaoErrorOut = g_fHuandaoTurn *  (float) ((AD_Data.AD_R_R -AD_Data.AD_L_L ) * 2 + (AD_Data.AD_R - AD_Data.AD_L) * 3) / 5.0f;//竖直电感差比和;
		if(fabs(GYROZ_SUM) > 45)
		{
			Flag_Huandao = 5;
		}
		if(fHuandaoErrorOut * Flag_Huandao_DIR>0) fHuandaoErrorOut = 0;
		if(fHuandaoErrorOut<35&&fHuandaoErrorOut>0)  fHuandaoErrorOut=35;
		if(fHuandaoErrorOut>-35&&fHuandaoErrorOut<0)  fHuandaoErrorOut=-35;
		if(fHuandaoErrorOut >250) fHuandaoErrorOut =250;
		if(fHuandaoErrorOut < -250) fHuandaoErrorOut = -250;
	}
		
	
	if(Flag_Huandao == 5 && fabs(GYROZ_SUM) > g_nHuandaoAnger)	//准备出环
	{
		Flag_Huandao = 7;
		Huandao_Timer = Huandao_Timer*0.15;
		
	}
	if(Flag_Huandao == 7)
	{
		if(Huandao_Timer--< 0)
		{		
			Flag_Huandao = 9;
			Huandao_Rise = 0;
			GYROZ_SUM = 0;	
		}
	}
	if(Flag_Huandao == 9 &&AD_Data.AD_R < 20&&  AD_Data.AD_L < 20&& AD_Data.AD_R_R  < 30 && AD_Data.AD_L_L < 30)//判断是否出环，清零环岛标志位以便下次判断
	{
   	Flag_Huandao = 0;
		Huandao_Timer =0;
		Flag_Huandao_DIR=0;
		Huandao_time++;
	}
	
	if(Flag_Huandao == 3)
	{
		g_fDirectionError = fHuandaoErrorOut;
	}
	else if(Flag_Huandao == 7)
	{
		if(Flag_zitai ==0)
		{
			g_fDirectionError = fHuandaoErrorOut;
		}
		else
		{
			if( Flag_Huandao_DIR==-1)
			g_fDirectionError =  100 * ((float)(AD_Data.AD_M-(AD_Data.AD_L_L*2+AD_Data.AD_L)) / (float)(AD_Data.AD_M+AD_Data.AD_L_L));
			if( Flag_Huandao_DIR==1)
			g_fDirectionError = 100 * ((float)((AD_Data.AD_R_R*2+AD_Data.AD_R)-AD_Data.AD_M) / (float)(AD_Data.AD_M+AD_Data.AD_R_R));
		}
		if(g_fDirectionError * Flag_Huandao_DIR<0) 
		{
			if(Flag_zitai==0)
		  g_fDirectionError *=0.05f;
			else if(Flag_zitai==1)
			g_fDirectionError *=0.2f;	
		}
	}
	else
	{
		g_fDirectionError = chabihe;
	}
	
	//最终误差输出值--->到转向环
if(Hengduan_flag!= 0)
	{ 
		g_fHengduan_Error	=	Remeber_yaw + Hengduan_angle;
		if(g_fHengduan_Error>90&&yaw<0)
		{			
			  yaw=yaw+360;
		}
		if(g_fHengduan_Error<-90&&yaw>0)
		{			
			  yaw=yaw-360;
		}
		 Radius=PID_Positional( &Track_HengDuan, yaw,g_fHengduan_Error );//横断
	}
	else
	{		
		if(Flag_zitai==0)
		Radius=PID_Positional( &ZL_Track_PID, g_fDirectionError  , 0);
		else
		Radius=PID_Positional( &SL_Track_PID, g_fDirectionError  , 0);	
	}
	return SUCCESS;
}
***********************************/
