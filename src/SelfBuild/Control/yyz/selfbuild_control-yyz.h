#ifndef _SELFBUILD_CONTROL_H_
#define _SELFBUILD_CONTROL_H_

#include "headfile.h"

#define Dead_Pwm	100		//死区PWM
#define PWM_MAX 	1000	//最大PWM
#define ZERO_Pitch	23		//机械零点

#define Huandao_In_Left    1
#define Huandao_Out_Left  -1
#define Huandao_In_Right   2
#define Huandao_Out_Right -2

extern uint8 System_OK;

typedef struct 	//电机相关数据结构体
{
  int32 Speed_Ave_Set;	
  
  char Left_Crazy;	// 电机疯转
	char Right_Crazy;	// 电机疯转
	
	char Crazy_StopFlag;
  
	int32 Speed_L;
	int32 Speed_L_Last;
	int32 Left_Acc;
	int32 Speed_R;
	int32 Speed_R_Last;
	int32 Right_Acc;
	
	int32 Speed_Ave_Now;
	int32 Speed_Ave_Last;
	int32 Crazy_Count;
	
	char Car_Direct;		//车运动方向
	
	int32 PWM_R_Set;
	int32 PWM_L_Set;
	
	int32 PWM_R_Set_Last;
	int32 PWM_L_Set_Last;
}Motor_Data_Typedef;

extern Motor_Data_Typedef Motor_Data;	//电机相关变量

typedef struct
{
	uint8 Time_out,
				Steer_crazy,
				Wireless_Stop,
				Text_Stop;
}Error_Flag_Typedef;
extern Error_Flag_Typedef Error_Flag; //错误相关变量

uint8 Init_ALL(void);
uint8 Motor_PWM_Set(void);
uint8 Motor_Speed_Get(void);
void ALL_STOP_Judge(void);
void ALL_Run(void);

#endif