 /*****************************************************************************
  * @file        main.c
  * @company     ZUST Smart Car
  * @author      HAOMING CHEN
  * @version
  * @software		 MDK
  * @core				 LPC54606J512BD100
  * @date        2019-10-18
  * @note
  ***************************************************************************/


#include "headfile.h"



int main(void)
{
	init_all();


	Motor.PWM_L_Set = 500;
	Motor.PWM_R_Set = 500;
	gpio_set(A23, 0);
	while(1)
	{		
//		OLED_P6x8Str(0,0,"didi");OLED_P6x8Int(60,0,Motor.Speed_Ave_Now, -4);
//		OLED_P6x8Flo(0,1,MPU6050.Target_Angle.y,-4);
//		
//		OLED_P6x8Str(0,2,"Diff_Ratio");oled_float(60,2,AD_Data.Diff_Ratio,3,2);
//		
//		OLED_P6x8Str(0,0,"Radius");OLED_P6x8Int(60,0,Radius,-3);
//		
//		OLED_P6x8Str(0,1,"Speed");OLED_P6x8Int(60,1,Speed_Min,-4);

//		OLED_P6x8Str(0,2,"Direct");OLED_P6x8Int(60,2,Direct_Parameter,-4);


		OLED_P6x8Str(0,2,"cnm");

		gpio_set(A23, 1);
		OLED_P6x8Int(0,1,Motor.Speed_L,-4);
		gpio_set(A23, 0);
		OLED_P6x8Int(0,2,Motor.Speed_R,-4);

		PWM_Set();
		UI_Send();

		
	}


}


