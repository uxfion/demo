#include "selfbuild_control.h"


////   (0.21*(speed/(512*2.25)))/0.001	速度换算
////  速度设定 100->1.8m/s
////			 120->2.2m/s
//// 			 140->2.5m/s
////			 160->3m/s			<-------目标

//extern uint16 Stop_Time;

//Motor_Data_Typedef Motor_Data={0};
//Error_Flag_Typedef Error_Flag={0};

//uint8 Init_ALL(void)		//初始化
//{
//  /*----------------------------数据初始化--------------------------------*/
//  memset(&Motor_Data,0,sizeof(Motor_Data));//初始化所有结构体成员为0
//  memset(&AD_Data,0,sizeof(AD_Data));
//  memset(&MPU6050,0,sizeof(MPU6050));
//  MPU6050.ZERO.x=0;
//  MPU6050.ZERO.y=ZERO_Pitch;	//零点角度
//  MPU6050.ZERO.z=0;
//  
//  /*----------------------------端口初始化--------------------------------*/
//  gpio_init(Button_Up,GPI,1,PULLUP);	
//  gpio_init(Button_Up, GPI, 1,PULLUP);
//  gpio_init(Button_Down, GPI, 1,PULLUP);
//  gpio_init(Button_Left, GPI, 1,PULLUP);
//  gpio_init(Button_Right, GPI, 1,PULLUP);
//  gpio_init(Button_Mid, GPI, 1,PULLUP);
//  gpio_init(Beep, GPO, 0,PULLUP);
//  gpio_init(Switch_2, GPI, 1,PULLUP);
//  gpio_init(Switch_3, GPI, 1,PULLUP);
//  gpio_init(Switch_4, GPI, 1,PULLUP);
////  gpio_init(Time_Test,GPO,1,PULLUP);	//时间测试端口
//  
//  /*----------------------------OLED初始化--------------------------------*/
//  oled_init(); 	
//   /*----------------------------摄像头初始化--------------------------------*/

//#if MV9V032CameraEnable  == 1 //定义在PORT_cfg.h
//	camera_init();		//总钻风初始化
//#endif
//  
//  /*----------------------------数字电位器初始化--------------------------*/
////  Correct_Sensor();
//  
//  /*----------------------------菜单调参----------------------------------*/
////  DisableInterrupts;                          //关闭所有中断，防止菜单调节过程中出现中断
////  Menu_Init();                                  //初始化菜单
////  while(!Menu_Work()) systick_delay_ms(200);    //菜单每200ms工作一次，并根据是否按下“关闭菜单”选项后（函数返回0）结束死循环
////  EnableInterrupts;
//  
// /*----------------------------编码器初始化------------------------------*/
//	  ctimer_count_init(CODER_L);	
//		ctimer_count_init(CODER_R);
//		gpio_init(CODER_DIR_L, GPI, 1,PULLUP);
//		gpio_init(CODER_DIR_R, GPI, 1,PULLUP);
///*----------------------------电机初始化-------------------------------*/
//		sct_pwm_init(PWM_CH_R_Z, 7200, 0);	
//		sct_pwm_init(PWM_CH_R_F, 7200, 0);
//		sct_pwm_init(PWM_CH_L_Z, 7200, 0);
//		sct_pwm_init(PWM_CH_L_F, 7200, 0);
//  
//  /*----------------------------六轴初始化--------------------------------*/ 
//  while(MPU6050_ADDDMPInit());	//初始化DMP 
//  while(MPU6050_SelfTest());
//  OLED_ClearScreen(BLACK); 
//  OLED_P6x8Str(0, 0, "pitch=");
//  OLED_P6x8Str(0, 1, "roll=");
//  OLED_P6x8Str(0, 2, "yaw=");
//  while(1)
//  {
//		  Refresh_MPUTeam(DMP);
//		  OLED_P6x8Flo(50, 0, MPU6050.Pitch, -3);
//		  OLED_P6x8Flo(50, 1, MPU6050.Roll, -3);
//		  OLED_P6x8Flo(50, 2, MPU6050.Yaw, -3);
//		  if(gpio_get(Button_Up) == 0)
//		  {
//		  while(!System_OK)
//		  {
//			  Refresh_MPUTeam(DMP);
//			  if(MPU6050.Pitch>40)
//			  System_OK=1;
//		  }
//		  OLED_ClearScreen(BLACK); 
//		  break;
//		  }
//  }

///*----------------------------定时器中断初始化--------------------------------*/
//  pit_init_ms(2);
//	set_irq_priority(RIT_IRQn,0);//设置优先级 越低优先级越高
//	//中断函数void RIT_DriverIRQHandler(void)在isr.c文件内		
// /*----------------------------无线调试器初始化--------------------------------*/
//  wireless_CommunInit();
///*-------------------------------------初始化全部结束--------------------------------------------------------------------------------*/ 
//  OLED_ClearScreen(BLACK);
//  return SUCCESS;
//}

///////////////////////////////////////////////////////坡道加速待添加/////////////////////////////////////////////////////////////
//uint8 Motor_PWM_Set(void)	//PWM输出
//{
//  Motor_Data.PWM_L_Set+=Dead_Pwm;	//去除死区PWM
//  Motor_Data.PWM_R_Set+=Dead_Pwm;
//  Motor_Data.PWM_L_Set=RANGE(Motor_Data.PWM_L_Set,PWM_MAX,-PWM_MAX);		//PWM限幅
//  Motor_Data.PWM_R_Set=RANGE(Motor_Data.PWM_R_Set,PWM_MAX,-PWM_MAX);
//  sct_pwm_duty(PWM_CH_L_Z,((Motor_Data.PWM_L_Set>0)?Motor_Data.PWM_L_Set:0));	//PWM输出
//  sct_pwm_duty(PWM_CH_L_F,((Motor_Data.PWM_L_Set<0)?(-Motor_Data.PWM_L_Set):0)); 
//  sct_pwm_duty(PWM_CH_R_Z,((Motor_Data.PWM_R_Set>0)?Motor_Data.PWM_R_Set:0));
//  sct_pwm_duty(PWM_CH_R_F,((Motor_Data.PWM_R_Set<0)?(-Motor_Data.PWM_R_Set):0));
//  return SUCCESS;
//}

//uint8 Motor_Speed_Get(void)	//得到电机速度（编码器）
//{
//#if MV9V032CameraEnable == 1  //定义在PORT_cfg.h
//    //使用摄像头
//		Motor_Data.Speed_L=gpio_get(CODER_DIR_L)?ctimer_count_read(CODER_L):-ctimer_count_read(CODER_L);
//		ctimer_count_clean(CODER_L);
//		Motor_Data.Speed_R=gpio_get(CODER_DIR_R)?ctimer_count_read(CODER_R):-ctimer_count_read(CODER_R);
//    ctimer_count_clean(CODER_R);
//#endif
//	/******* 右电机速度相关控制 ********/

//	Motor_Data.Right_Acc = Motor_Data.Speed_R - Motor_Data.Speed_R_Last;	// 计算加速度
//	if (Motor_Data.Right_Acc  > 100)
//	{
//		Motor_Data.Right_Crazy = 1;	// 疯转
//	}
//	if (Motor_Data.Speed_R > Motor_Data.Speed_Ave_Set + 200)
//	{
//		Motor_Data.Right_Crazy = 2;	// 疯转
//	}
//	if (Motor_Data.Speed_R < -350)
//	{
//		Motor_Data.Right_Crazy = -1;	// 倒转
//	}
//	
//	if (Motor_Data.Right_Crazy)
//	{
//		if (Motor_Data.Right_Acc  <= 100)
//		{
//			if (Motor_Data.Speed_R < Motor_Data.Speed_Ave_Set + 200 && Motor_Data.Speed_R > 0)
//			{
//				Motor_Data.Right_Crazy = 0;
//			}
//		}
//	}
//	
//	if (!Motor_Data.Right_Crazy)
//	{
//		Motor_Data.Speed_R = Motor_Data.Speed_R*0.9 + Motor_Data.Speed_R_Last*0.1;
//		Motor_Data.Speed_R_Last = Motor_Data.Speed_R;	// 更新右轮速度
//	}
//	else
//	{
//		Motor_Data.Speed_R = Motor_Data.Speed_R*0.5 + Motor_Data.Speed_R_Last*0.5;
//		Motor_Data.Speed_R_Last = Motor_Data.Speed_R;	// 更新右轮速度
//	}
//	/******* 右电机速度相关控制结束 ********/
//	
//	/******* 左电机速度相关控制 ********/
//	
//	Motor_Data.Left_Acc = Motor_Data.Speed_L - Motor_Data.Speed_L_Last;	// 计算加速度
//	if (Motor_Data.Left_Acc > 100)
//	{
//		Motor_Data.Left_Crazy = 1;
//	}
//	if (Motor_Data.Speed_L > Motor_Data.Speed_Ave_Set + 200)
//	{
//		Motor_Data.Left_Crazy = 2;
//	}
//	if (Motor_Data.Speed_L < -350)
//	{
//		Motor_Data.Left_Crazy = -1;
//	}
//	
//	if (Motor_Data.Left_Crazy)
//	{
//		if (Motor_Data.Left_Acc <= 100)
//		{
//			if (Motor_Data.Speed_L < Motor_Data.Speed_Ave_Set + 200 && Motor_Data.Speed_L > 0)
//			{
//				Motor_Data.Left_Crazy = 0;
//			}
//		}
//	}
//	
//	if (!Motor_Data.Left_Crazy)
//	{
//		Motor_Data.Speed_L = 0.9*Motor_Data.Speed_L + 0.1*Motor_Data.Speed_L_Last;	// 低通滤波
//		Motor_Data.Speed_L_Last = Motor_Data.Speed_L;	// 更新左轮速度
//	}
//	else
//	{
//		Motor_Data.Speed_L = 0.5*Motor_Data.Speed_L + 0.5*Motor_Data.Speed_L_Last;	// 低通滤波
//		Motor_Data.Speed_L_Last = Motor_Data.Speed_L;	// 更新左轮速度
//	}

//	
//	
//	/******* 左电机速度相关控制结束 */
//	
//	if ((Motor_Data.Left_Crazy && Motor_Data.Right_Crazy) || (Motor_Data.Left_Crazy && Motor_Data.Speed_R < 20) || (Motor_Data.Right_Crazy && Motor_Data.Speed_L < 20))
//	{
//		Motor_Data.Crazy_Count++;
//		if (Motor_Data.Crazy_Count >= 40)
//		{
//			Motor_Data.Crazy_Count 	= 0;
//			Motor_Data.Crazy_StopFlag = 1;
//		}
//	}
//	else
//	{
//		Motor_Data.Right_Crazy = 0;
//	}
//	
//	/******* 电机疯转特殊处理 ********/
//	if ((Motor_Data.Left_Crazy > 0) && (Motor_Data.Right_Crazy > 0))
//	{
//		Motor_Data.Speed_Ave_Now = Motor_Data.Speed_Ave_Set;			// 两边都疯转，使用上次速度作为当前实际速度
//	}
//	else if (Motor_Data.Left_Crazy)
//	{
//		if (Motor_Data.Speed_R > Motor_Data.Speed_Ave_Set)
//		{
//			Motor_Data.Speed_Ave_Now = Motor_Data.Speed_Ave_Set;
//		}
//		else
//		{
//			Motor_Data.Speed_Ave_Now = Motor_Data.Speed_R;	// 左电机疯转，使用上次速度作为当前实际速度
//		}
//	}
//	else if (Motor_Data.Right_Crazy)
//	{
//		if (Motor_Data.Speed_L > Motor_Data.Speed_Ave_Set)
//		{
//			Motor_Data.Speed_Ave_Now = Motor_Data.Speed_Ave_Set;
//		}
//		else
//		{
//			Motor_Data.Speed_Ave_Now = Motor_Data.Speed_L;	// 右电机疯转，使用上次速度作为当前实际速度
//		}
//	}
//	else
//	{
//		Motor_Data.Speed_Ave_Now = (Motor_Data.Speed_L + Motor_Data.Speed_R) / 2;	// 左右取平均计算车子实际速度
//	}
//	
//	Motor_Data.Speed_Ave_Now = Motor_Data.Speed_Ave_Now *0.9 + Motor_Data.Speed_Ave_Last * 0.1;
//	Motor_Data.Speed_Ave_Last = Motor_Data.Speed_Ave_Now;
//	return SUCCESS;
//}

//void ALL_STOP_Judge(void)		//系统停止判断
//{
//  if(Error_Flag.Steer_crazy ==0 && Error_Flag.Time_out == 0 && Error_Flag.Wireless_Stop == 0 && Error_Flag.Text_Stop==0)
//	{
//	  return;
//	}
//	OLED_ClearScreen(BLACK);
//  disable_irq(RIT_IRQn);
//	sct_pwm_duty(PWM_CH_L_Z,0);	//PWM输出
//  sct_pwm_duty(PWM_CH_L_F,0); 
//  sct_pwm_duty(PWM_CH_R_Z,0);
//  sct_pwm_duty(PWM_CH_R_F,0);
//	Motor_Data.Speed_Ave_Set=0;
//	Wireless_Flag.FLAG_ALLSTOP=0;
//	Wireless_Flag.FLAG_START=0;
//	Wireless_Flag.FLAG_GO=0;
//	Wireless_Flag.FLAG_BACK=0;
//	Wireless_Flag.FLAG_Speedzero=0;
//	
//	if(Error_Flag.Wireless_Stop == 1)
//	{
//	  Error_Flag.Wireless_Stop = 0;
//	  OLED_P6x8Str(OLED_SHOW(1), 1,"Stop cmd!!!");
//	  OLED_P6x8Str(OLED_SHOW(1), 2,"Send signal to");
//	  OLED_P6x8Str(OLED_SHOW(1), 3,"continue!");
//	  while(!Wireless_Flag.FLAG_START);	//	直到接收到开始标志位
//	  enable_irq(RIT_IRQn);   	//开启直立控制
//	}
//	if(Error_Flag.Time_out == 1)
//	{
//	  Error_Flag.Time_out = 0;
//	  OLED_P6x8Int(OLED_SHOW(1), 1, Stop_Time, 4); 
//	  OLED_P6x8Str(OLED_SHOW(2), 1,"Time Out!!!");
//	  OLED_P6x8Str(OLED_SHOW(1), 2,"Press Button up to");
//	  OLED_P6x8Str(OLED_SHOW(1), 3,"continue!");
//	  while(gpio_get(Button_Up) == 1);		//直到按下上键退出并启动
//	  ALL_Run();//系统启动
//	}
//	if(Error_Flag.Steer_crazy == 1)
//	{
//	   Error_Flag.Steer_crazy=0;
//	   OLED_P6x8Str(OLED_SHOW(1), 1,"Motor Crazy!!!");
//	   OLED_P6x8Str(OLED_SHOW(1), 2,"Press Reset to");
//	   OLED_P6x8Str(OLED_SHOW(1), 3,"continue!");
//	   while(1);		//复位才可解决此错误
//	}
//	
//	if(Error_Flag.Text_Stop == 1)
//	{
//	  Error_Flag.Text_Stop = 0;
//	  OLED_P6x8Str(OLED_SHOW(2), 1,"Text_Stop!!!");
//	  OLED_P6x8Str(OLED_SHOW(1), 2,"Press Button up to");
//	  OLED_P6x8Str(OLED_SHOW(1), 3,"continue!");
//	  while(gpio_get(Button_Up) == 1);		//直到按下上键退出并启动
//	  ALL_Run();//系统启动
//	}
//	OLED_ClearScreen(BLACK);
//}

//void ALL_Run(void)		//系统启动
//{
//	 enable_irq(RIT_IRQn);  //开启直立控制
//	 Motor_Data.Speed_Ave_Set=Speed_Set;
//}
